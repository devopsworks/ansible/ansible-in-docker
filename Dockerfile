FROM debian:buster-slim

ARG ANSIBLE_MIN_VERSION=2.3
ARG ANSIBLE_MAX_PYTHON2_VERSION=2.5
ARG ANSIBLE_CURRENT_VERSION_TO_INSTALL=2.9

ENV DEBIAN_FRONTEND=noninteractive

ADD entrypoint.sh /usr/local/bin/entrypoint.sh

RUN apt-get update \
  && ( \
      if [ 1 -eq "$(echo ${ANSIBLE_CURRENT_VERSION_TO_INSTALL} ${ANSIBLE_MAX_PYTHON2_VERSION}  | awk '{if ($1 < $2) print 1; else print 0}')" ]; then \
       apt-get install -y python-dev python-pip python-setuptools python-openssl python-crcmod sshpass git; \
      else \
       apt-get install -y python3-dev python3-pip python3-setuptools python3-openssl python3-crcmod sshpass git; \
        update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1; \
        update-alternatives --install /usr/bin/python python /usr/bin/python3 1; \
      fi \
      ) \
  && rm -rf /var/lib/apt/lists/* \
  && pip install -q --no-cache-dir ansible-inventory-grapher ansible-lint ansible~=${ANSIBLE_CURRENT_VERSION_TO_INSTALL}.0 apache-libcloud appdirs boto boto3 docker-py httplib2 ipaddr Jinja2 molecule netaddr packaging paramiko python-vagrant PyYAML six yamllint \
  && chmod +x /usr/local/bin/entrypoint.sh

CMD [ "ansible", "--help" ]
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
